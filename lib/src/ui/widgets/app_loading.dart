import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../data/local/get_storage_helper.dart';
import 'package:shimmer/shimmer.dart';
import '../../configs/assets.dart';
import '../../configs/colors.dart';

class AppLoading extends GetResponsiveView {
  @override
  builder() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Shimmer.fromColors(
              period: Duration(milliseconds: 1500),
              direction: ShimmerDirection.ltr,
              baseColor: Get.find<GetStorageHelper>().isDarkMode
                  ? Colors.grey.shade900
                  : AppColors.purpule,
              highlightColor: AppColors.red,
              child: SizedBox(
                width: Get.width * 0.5,
                height: Get.height * 0.5,
                child: Image.asset(Assets.appIcon),
              )),
        ),
      ],
    );
  }
}
