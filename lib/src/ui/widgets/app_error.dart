import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../configs/colors.dart';

class AppError extends GetResponsiveView {
  final String? errorMessage;
  final Function? reload;
  AppError({this.errorMessage, this.reload});
  @override
  builder() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
              width: Get.width * 0.9,
              child: Center(
                  child: Text(
                errorMessage!,
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: Get.width * 0.05),
              ))),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                Get.back();
              },
              child: Container(
                height: Get.width * 0.1,
                width: Get.width * 0.2,
                decoration: BoxDecoration(
                    // color: AppColors.orange.withOpacity(0.7),
                    gradient: AppColors.linearGradient,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                    )),
              ),
            ),
            SizedBox(
              width: Get.width * 0.08,
            ),
            InkWell(
              onTap: () async {
                await reload!();
              },
              child: Container(
                height: Get.width * 0.1,
                width: Get.width * 0.2,
                decoration: BoxDecoration(
                    gradient: AppColors.linearGradient,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Icon(
                      Icons.replay_outlined,
                    )),
              ),
            ),
          ],
        )
      ],
    );
  }
}
