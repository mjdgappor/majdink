import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../configs/colors.dart';

class MainSnackBar {
  static mainSnack(
      {String title = '',
      String body = '',
      SnackPosition? snackPosition = SnackPosition.BOTTOM}) {
    if (Get.isSnackbarOpen) {
      Get.closeAllSnackbars();
    }
    Get.snackbar(title, body,
        snackPosition: snackPosition,
        colorText: Colors.white,
        backgroundColor: AppColors.purpule.withOpacity(0.8),
        backgroundGradient: AppColors.linearGradient);
  }
}
