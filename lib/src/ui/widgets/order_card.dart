import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';
import '../../configs/assets.dart';
import '../../configs/strings.dart';
import '../../configs/colors.dart';
import '../../utils/main_notification.dart';
import '../../data/local/get_storage_helper.dart';
import '../../data/models/order_model.dart';
import '../screens/detailes/detailes_page.dart';

class OrderCard extends GetResponsiveView {
  OrderCard({Key? key, this.order, this.isFavorate}) : super(key: key);
  Order? order;
  RxBool? isFavorate = false.obs;

  @override
  Widget builder() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: GestureDetector(
          onTap: () {
            Get.toNamed(DetailesPage.route, arguments: order);
          },
          child: Container(
            child: Stack(
              alignment: AlignmentDirectional.centerStart,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.blue.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 5,
                        offset: const Offset(0, 1),
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Spacer(
                        flex:
                            screen.context.orientation == Orientation.landscape
                                ? 5
                                : 4,
                      ),
                      Container(
                        color: Colors.white,
                        height:
                            screen.context.orientation == Orientation.portrait
                                ? Get.height * 0.12
                                : Get.height * 0.24,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GradientText(
                                "${Strings.number.tr} : " + order!.id!,
                                colors: [
                                  AppColors.red,
                                  AppColors.purpule,
                                  AppColors.blue
                                ],
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                              ),
                              GradientText(
                                "${Strings.date.tr} : ${order!.createdAt!.year}/${order!.createdAt!.month}/${order!.createdAt!.day} \n${Strings.time.tr} : ${order!.createdAt!.hour}:${order!.createdAt!.minute}:${order!.createdAt!.second}",
                                colors: [
                                  AppColors.red,
                                  AppColors.purpule,
                                  AppColors.blue
                                ],
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                              ),
                              GradientText(
                                "${Strings.total.tr} : ${order!.total!} ${order!.currency!}",
                                colors: [
                                  AppColors.red,
                                  AppColors.purpule,
                                  AppColors.blue
                                ],
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                              )
                            ],
                          ),
                        ),
                      ),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                            onTap: () async {
                              isFavorate!.value = !isFavorate!.value;
                              order!.isFavorite = isFavorate!.value;
                              if (isFavorate!.value == true) {
                                Get.find<GetStorageHelper>()
                                    .addToFavorites(order!);

                                Get.find<MainNotification>()
                                    .sendNotification(order!);
                              } else {
                                Get.find<GetStorageHelper>()
                                    .removeFromFavorites(order!);
                                Get.forceAppUpdate();
                                await AudioPlayer()
                                    .play(AssetSource(Assets.chimeupSound));
                              }
                            },
                            child: Obx(
                              () => Icon(
                                isFavorate!.value
                                    ? Icons.favorite
                                    : Icons.favorite_border,
                                size: screen.context.orientation ==
                                        Orientation.portrait
                                    ? Get.width * 0.1
                                    : Get.width * 0.05,
                                color: isFavorate!.value
                                    ? AppColors.purpule
                                    : Colors.grey,
                              ),
                            )),
                      ),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.8),
                        spreadRadius: 2,
                        blurRadius: 5,
                        offset: const Offset(0, 1),
                      ),
                    ],
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: CachedNetworkImage(
                      imageUrl: order!.image!,
                      height: screen.context.orientation == Orientation.portrait
                          ? Get.height * 0.15
                          : Get.height * 0.3,
                      width: screen.context.orientation == Orientation.portrait
                          ? Get.width * 0.35
                          : Get.width * 0.17,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
