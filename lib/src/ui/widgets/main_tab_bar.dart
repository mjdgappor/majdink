import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';
import 'package:test_inkcode/src/configs/strings.dart';
import '../../configs/colors.dart';
import '../screens/home/home_controller.dart';

class MainTabBar extends GetResponsiveView<HomeController> {
  Widget builder() {
    return Container(
      child: Obx(() => Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () {
                  controller.index.value = 0;
                },
                child: Container(
                    width: Get.width * 0.4,
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: controller.index.value == 0
                                    ? AppColors.purpule
                                    : Colors.grey))),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GradientText(
                        Strings.home.tr,
                        textAlign: TextAlign.center,
                        colors: controller.index.value == 0
                            ? [AppColors.red, AppColors.purpule, AppColors.blue]
                            : [Colors.grey, Colors.grey],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: Get.width * 0.06),
                      ),
                    )),
              ),
              GestureDetector(
                onTap: () {
                  controller.index.value = 1;
                },
                child: Container(
                  width: Get.width * 0.4,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: controller.index.value == 1
                                  ? AppColors.purpule
                                  : Colors.grey))),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GradientText(
                      Strings.favorites.tr,
                      colors: controller.index.value == 1
                          ? [AppColors.red, AppColors.purpule, AppColors.blue]
                          : [Colors.grey, Colors.grey],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: Get.width * 0.06),
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
