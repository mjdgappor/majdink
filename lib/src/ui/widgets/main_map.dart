import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:latlong2/latlong.dart';
import '../../configs/colors.dart';

class MainMap extends GetResponsiveView {
  final RxDouble? lat;
  final RxDouble? long;

  final String? title;
  final MapController? mapController;

  MainMap({Key? key, this.lat, this.long, this.title, this.mapController})
      : super(key: key);
  @override
  Widget builder() {
    return Obx(() => FlutterMap(
          options: MapOptions(
            allowPanningOnScrollingParent: false,
            center: LatLng(lat!.value, long!.value),
            zoom: 5,
          ),
          layers: [
            TileLayerOptions(
              //urlTemplate: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c'],
              retinaMode: true,
            ),
            MarkerLayerOptions(
              markers: [
                Marker(
                  width: 30.0,
                  height: 30.0,
                  point: LatLng(lat!.value, long!.value),
                  builder: (ctx) => Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(),
                      child: Opacity(
                        opacity: 0.8,
                        child: Container(
                            alignment: Alignment.bottomCenter,
                            child: Icon(
                              Icons.location_on,
                              size: Get.width * 0.09,
                              color: AppColors.red,
                              shadows: [
                                BoxShadow(
                                  color: AppColors.blue.withOpacity(1),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(0, 1),
                                ),
                              ],
                            )),
                      )),
                ),
              ],
            ),
          ],
        ));
  }
}
