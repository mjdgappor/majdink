import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import '../../../configs/strings.dart';
import '../../../configs/colors.dart';
import '../../../data/models/api_response.dart';
import '../../widgets/app_error.dart';
import '../../widgets/app_loading.dart';
import '../../widgets/main_tab_bar.dart';
import '../../../data/local/get_storage_helper.dart';
import '../../../data/models/order_model.dart';
import 'home_controller.dart';
import '../../widgets/order_card.dart';

class HomePage extends GetResponsiveView<HomeController> {
  static const route = "/HomePage";
  HomePage({Key? key}) : super(key: key);

  @override
  Widget builder() {
    return Scaffold(
        floatingActionButton: GestureDetector(
          onTap: () async {
            Get.locale == const Locale('ar')
                ? await Get.updateLocale(const Locale('en'))
                : await Get.updateLocale(const Locale('ar'));
            Get.find<GetStorageHelper>()
                .changeLanguage(Get.locale!.languageCode);
          },
          child: Container(
              decoration: BoxDecoration(
                gradient: AppColors.linearGradient,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.8),
                    spreadRadius: 3,
                    blurRadius: 5,
                    offset: const Offset(0, 1),
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Icon(
                  Icons.translate_rounded,
                  color: Colors.white,
                ),
              )),
        ),
        body: Obx(() {
          if (controller.getOrdersResponse.value.status == Status.LOADING ||
              controller.isLoading.value == true) {
            return AppLoading();
          }
          if (controller.getOrdersResponse.value.status == Status.ERROR) {
            return AppError(
              errorMessage: controller.getOrdersResponse.value.message,
              reload: () async => await controller.getOrders(),
            );
          }
          return Stack(
            children: [
              Column(
                children: [
                  SafeArea(child: MainTabBar()),
                  SizedBox(
                    height: Get.height * 0.01,
                  ),
                  Obx(() {
                    if (controller.index.value == 0) {
                      return NotificationListener<ScrollNotification>(
                        onNotification: (notification) {
                          if (notification is ScrollEndNotification) {
                            if (notification.metrics.pixels ==
                                notification.metrics.maxScrollExtent) {
                              controller.getOrdersLive();
                              return true;
                            }
                          }
                          return true;
                        },
                        child: controller.orderList.isEmpty
                            ? Column(
                                children: [
                                  Center(child: Text(Strings.noOrders.tr)),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: InkWell(
                                      onTap: () async {
                                        controller.onInit();
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            boxShadow: [
                                              BoxShadow(
                                                color: AppColors.blue
                                                    .withOpacity(0.5),
                                                spreadRadius: 1,
                                                blurRadius: 2,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                            gradient: AppColors.linearGradient,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Directionality(
                                            textDirection: TextDirection.rtl,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Icon(
                                                Icons.replay_outlined,
                                                color: Colors.white,
                                              ),
                                            )),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            : Expanded(
                                child: screen.context.orientation ==
                                        Orientation.landscape
                                    ? SafeArea(
                                        child: AnimationLimiter(
                                          child: GridView.builder(
                                            gridDelegate:
                                                const SliverGridDelegateWithFixedCrossAxisCount(
                                                    crossAxisCount: 2,
                                                    crossAxisSpacing: 2.0,
                                                    mainAxisSpacing: 20,
                                                    childAspectRatio: 2.7),
                                            shrinkWrap: true,
                                            primary: false,
                                            itemCount:
                                                controller.orderList.length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return AnimationConfiguration
                                                  .staggeredList(
                                                position: index,
                                                duration: const Duration(
                                                    milliseconds: 800),
                                                child: SlideAnimation(
                                                  verticalOffset: 50.0,
                                                  child: FadeInAnimation(
                                                      child: OrderCard(
                                                    order: controller
                                                        .orderList[index],
                                                    isFavorate: Get.find<
                                                                GetStorageHelper>()
                                                            .favorites
                                                            .where((element) =>
                                                                Order.fromJson(
                                                                        element)
                                                                    .id ==
                                                                controller
                                                                    .orderList[
                                                                        index]
                                                                    .id)
                                                            .isNotEmpty
                                                        ? true.obs
                                                        : false.obs,
                                                  )),
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      )
                                    : AnimationLimiter(
                                        child: ListView.builder(
                                        // padding: EdgeInsets.all(8),
                                        shrinkWrap: true,
                                        primary: false,
                                        itemCount: controller.orderList.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return AnimationConfiguration
                                              .staggeredList(
                                            position: index,
                                            duration: const Duration(
                                                milliseconds: 800),
                                            child: SlideAnimation(
                                              verticalOffset: 50.0,
                                              child: FadeInAnimation(
                                                  child: OrderCard(
                                                order:
                                                    controller.orderList[index],
                                                isFavorate:
                                                    Get.find<GetStorageHelper>()
                                                            .favorites
                                                            .where((element) =>
                                                                Order.fromJson(
                                                                        element)
                                                                    .id ==
                                                                controller
                                                                    .orderList[
                                                                        index]
                                                                    .id)
                                                            .isNotEmpty
                                                        ? true.obs
                                                        : false.obs,
                                              )),
                                            ),
                                          );
                                        },
                                      )),
                              ),
                      );
                    }
                    if (Get.find<GetStorageHelper>().favorites.obs.isEmpty) {
                      return Center(
                          child: Text(Strings.noOrdersInFavourites.tr));
                    } else {
                      return screen.context.orientation == Orientation.landscape
                          ? SafeArea(
                              top: false,
                              child: AnimationLimiter(
                                child: GridView.builder(
                                  gridDelegate:
                                      const SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          crossAxisSpacing: 2.0,
                                          mainAxisSpacing: 20,
                                          childAspectRatio: 2.7),
                                  shrinkWrap: true,
                                  primary: false,
                                  itemCount: Get.find<GetStorageHelper>()
                                      .favorites
                                      .obs
                                      .length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return AnimationConfiguration.staggeredList(
                                      position: index,
                                      duration:
                                          const Duration(milliseconds: 800),
                                      child: SlideAnimation(
                                        verticalOffset: 50.0,
                                        child: FadeInAnimation(
                                            child: OrderCard(
                                          order: Order.fromJson(
                                              Get.find<GetStorageHelper>()
                                                  .favorites
                                                  .obs[index]),
                                          isFavorate: true.obs,
                                        )),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            )
                          : Expanded(
                              child: SafeArea(
                                top: false,
                                child: AnimationLimiter(
                                    child: ListView.builder(
                                  // padding: EdgeInsets.all(0),
                                  shrinkWrap: true,
                                  primary: false,
                                  //controller: scrollController,
                                  itemCount: Get.find<GetStorageHelper>()
                                      .favorites
                                      .obs
                                      .length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return AnimationConfiguration.staggeredList(
                                      position: index,
                                      duration:
                                          const Duration(milliseconds: 800),
                                      child: SlideAnimation(
                                        verticalOffset: 50.0,
                                        child: FadeInAnimation(
                                            child: Column(
                                          children: [
                                            OrderCard(
                                              order: Order.fromJson(
                                                  Get.find<GetStorageHelper>()
                                                      .favorites
                                                      .obs[index]),
                                              isFavorate: true.obs,
                                            ),
                                          ],
                                        )),
                                      ),
                                    );
                                  },
                                )),
                              ),
                            );
                    }
                  }),
                ],
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Obx(() {
                  if (controller.getOrdersLiveResponse.value.status ==
                      Status.LOADING) {
                    return Container(
                      decoration: BoxDecoration(
                          gradient: AppColors.linearGradient,
                          shape: BoxShape.circle),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: CircularProgressIndicator(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    );
                  }
                  if (controller.getOrdersLiveResponse.value.status ==
                      Status.ERROR) {
                    return Container(
                      child: AppError(
                          errorMessage:
                              controller.getOrdersLiveResponse.value.message,
                          reload: () => controller.getOrdersLive()),
                    );
                  }
                  return SizedBox();
                }),
              )
            ],
          );
        }));
  }
}
