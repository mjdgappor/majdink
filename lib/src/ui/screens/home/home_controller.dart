import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import '../../../configs/strings.dart';
import '../../widgets/main_snackbar.dart';
import '../../../data/local/get_storage_helper.dart';
import '../../../data/models/api_response.dart';
import '../../../data/models/order_model.dart';
import '../../../data/models/orders_model.dart';
import '../../../data/models/pagination_filter.dart';
import '../../../data/repository.dart';
import '../../../utils/dio_error_util.dart';

class HomeController extends GetxController {
  Rx<ApiResponse<OrderModel>> getOrdersResponse =
      ApiResponse<OrderModel>.completed(OrderModel()).obs;
  Rx<ApiResponse<OrderModel>> getOrdersLiveResponse =
      ApiResponse<OrderModel>.completed(OrderModel()).obs;
  Repository repository = Get.find();
  RxList<Order> orderList = <Order>[].obs;
  RxInt index = 0.obs;
  PaginationFilter paginationFilter = PaginationFilter();
  int pages = 0;
  RxBool isLoading = false.obs;

  Future getOrders() async {
    paginationFilter.page = 1;
    getOrdersResponse.value = ApiResponse<OrderModel>.loading('');
    try {
      var res = await repository.getOrders(paginationFilter: paginationFilter);

      if (res != null) {
        getOrdersResponse.value = ApiResponse<OrderModel>.completed(res);
        orderList.addAll(getOrdersResponse.value.data.data!);
        Get.find<GetStorageHelper>().removeFromOrders();

        Get.find<GetStorageHelper>().addToOrders(orderList);
        Get.find<GetStorageHelper>()
            .saveTotal(getOrdersResponse.value.data.paginate!.total!);
      }
    } on DioError catch (error) {
      getOrdersResponse.value =
          ApiResponse<OrderModel>.error(DioErrorUtil.handleError(error));
    }
  }

  Future getOrdersLive() async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == false) {
      MainSnackBar.mainSnack(
        title: Strings.pleaseMakeSureYourInternetConnection.tr,
      );
    } else {
      paginationFilter.page++;

      if (orderList.length == Get.find<GetStorageHelper>().total) {
        MainSnackBar.mainSnack(
          title: Strings.noMoreResults.tr,
        );
      } else {
        getOrdersLiveResponse.value = ApiResponse<OrderModel>.loading('');
        try {
          var res =
              await repository.getOrders(paginationFilter: paginationFilter);

          if (res != null) {
            getOrdersLiveResponse.value =
                ApiResponse<OrderModel>.completed(res);
            orderList.addAll(getOrdersLiveResponse.value.data.data!);
            Get.find<GetStorageHelper>().removeFromOrders();

            Get.find<GetStorageHelper>().addToOrders(orderList);
          }
        } on DioError catch (error) {
          getOrdersLiveResponse.value =
              ApiResponse<OrderModel>.error(DioErrorUtil.handleError(error));
          paginationFilter.page--;
        }
      }
    }
  }

  @override
  void onInit() async {
    isLoading.value = true;
    paginationFilter.page = 1;
    bool result = await InternetConnectionChecker().hasConnection;
    if (result == true) {
      await getOrders();
    } else {
      for (var element in Get.find<GetStorageHelper>().orders) {
        orderList.add(Order.fromJson(element));
      }
    }
    isLoading.value = false;
    super.onInit();
  }
}
