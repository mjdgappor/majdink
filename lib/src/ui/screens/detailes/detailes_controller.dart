import 'package:get/get.dart';
import '../../../data/local/get_storage_helper.dart';
import '../../../data/models/order_model.dart';

class DetailesController extends GetxController {
  Order order = Get.arguments;
  RxBool isFavorite = false.obs;

  @override
  void onInit() {
    isFavorite.value = Get.find<GetStorageHelper>()
            .favorites
            .where((element) => Order.fromJson(element).id == order.id)
            .isNotEmpty
        ? true
        : false;
    super.onInit();
  }
}
