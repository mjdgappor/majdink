import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';
import '../../../configs/assets.dart';
import '../../../configs/colors.dart';
import '../../../configs/strings.dart';
import '../../../data/local/get_storage_helper.dart';
import 'detailes_controller.dart';
import '../../widgets/main_map.dart';

class DetailesPage extends GetResponsiveView<DetailesController> {
  static const route = "/DetailesPage";

  DetailesPage({Key? key}) : super(key: key);

  @override
  Widget builder() {
    return Scaffold(
        body: SingleChildScrollView(
      child: SafeArea(
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.9),
                              spreadRadius: 1,
                              blurRadius: 5,
                              offset: const Offset(0, 1),
                            ),
                          ],
                          gradient: AppColors.linearGradient,
                          borderRadius: BorderRadius.circular(12)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          Strings.back.tr,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Center(
              child: Container(
                width: Get.width * 0.95,
                height: Get.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.8),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(0, 1),
                    ),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: SizedBox(
                    width: Get.width * 0.95,
                    height: Get.width,
                    child: MainMap(
                      lat: double.parse(controller.order.address!.lat!).obs,
                      long: double.parse(controller.order.address!.lng!).obs,
                      title: '',
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                        onTap: () {
                          controller.isFavorite.value =
                              !controller.isFavorite.value;
                          controller.order.isFavorite =
                              controller.isFavorite.value;
                          if (controller.isFavorite.value == true) {
                            Get.find<GetStorageHelper>()
                                .addToFavorites(controller.order);
                            Get.forceAppUpdate();
                          } else {
                            Get.find<GetStorageHelper>()
                                .removeFromFavorites(controller.order);
                            Get.forceAppUpdate();
                            AudioPlayer()
                                .play(AssetSource(Assets.chimeupSound));
                          }
                        },
                        child: Obx(
                          () => Icon(
                            controller.isFavorite.value
                                ? Icons.favorite
                                : Icons.favorite_border,
                            size: Get.width * 0.08,
                            color: controller.isFavorite.value
                                ? AppColors.purpule
                                : Colors.grey,
                          ),
                        )),
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: controller.order.items!.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      GradientText(
                                        "${Strings.item.tr} ${index + 1}    ",
                                        colors: [
                                          AppColors.red,
                                          AppColors.purpule,
                                          AppColors.blue
                                        ],
                                      ),
                                      GradientText(
                                        "${controller.order.items![index].name!}    ",
                                        colors: [
                                          AppColors.red,
                                          AppColors.purpule,
                                          AppColors.blue
                                        ],
                                      ),
                                    ],
                                  ),
                                  GradientText(
                                    " ${controller.order.items![index].price!}  ${controller.order.currency}",
                                    colors: [
                                      AppColors.red,
                                      AppColors.purpule,
                                      AppColors.blue
                                    ],
                                  )
                                ],
                              ),
                              Divider(
                                color: AppColors.purpule,
                              )
                            ],
                          ),
                        );
                      }),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GradientText(
                    "${Strings.total.tr}  ",
                    colors: [AppColors.red, AppColors.purpule, AppColors.blue],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GradientText(
                    "${controller.order.total!} ${controller.order.currency}",
                    colors: [AppColors.red, AppColors.purpule, AppColors.blue],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ));
  }
}
