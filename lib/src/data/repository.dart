import 'models/pagination_filter.dart';
import 'remote/api/app_api.dart';
import 'package:get/get.dart';

class Repository {
  AppApi appApi = Get.find();
  Future getOrders({PaginationFilter? paginationFilter}) =>
      appApi.getOrders(paginationFilter: paginationFilter);
}
