import 'dart:convert';
import 'order_model.dart';

OrderModel orderModelFromJson(String str) =>
    OrderModel.fromJson(json.decode(str));

String orderModelToJson(OrderModel data) => json.encode(data.toJson());

class OrderModel {
  OrderModel({
    this.data,
    this.code,
    this.message,
    this.paginate,
  });

  List<Order>? data;
  int? code;
  String? message;
  Paginate? paginate;

  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
        data: List<Order>.from(json["data"].map((x) => Order.fromJson(x))),
        code: json["code"],
        message: json["message"],
        paginate: Paginate.fromJson(json["paginate"]),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "code": code,
        "message": message,
        "paginate": paginate!.toJson(),
      };
}

class Paginate {
  Paginate({
    this.total,
    this.perPage,
  });

  int? total;
  int? perPage;

  factory Paginate.fromJson(Map<String, dynamic> json) => Paginate(
        total: json["total"],
        perPage: json["per_page"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "per_page": perPage,
      };
}
