class PaginationFilter {
  late int page;

  @override
  String toString() => 'PaginationFilter(page: $page)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is PaginationFilter && o.page == page;
  }

  @override
  int get hashCode => page.hashCode;
}
