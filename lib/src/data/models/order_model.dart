class Order {
  Order(
      {this.total,
      this.createdAt,
      this.image,
      this.currency,
      this.id,
      this.address,
      this.items,
      this.isFavorite});

  String? total;
  DateTime? createdAt;
  String? image;
  String? currency;
  String? id;
  Address? address;
  List<Item>? items;
  bool? isFavorite;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
      total: json["total"],
      createdAt: DateTime.parse(json["created_at"]),
      image: json["image"],
      currency: json["currency"],
      id: json["id"],
      address: Address.fromJson(json["address"]),
      items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
      isFavorite: false);

  Map<String, dynamic> toJson() => {
        "total": total,
        "created_at": createdAt!.toIso8601String(),
        "image": image,
        "currency": currency,
        "id": id,
        "address": address!.toJson(),
        "items": List<dynamic>.from(items!.map((x) => x.toJson())),
      };
}

class Address {
  Address({
    this.lat,
    this.lng,
  });

  String? lat;
  String? lng;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        lat: json["lat"],
        lng: json["lng"],
      );

  Map<String, dynamic> toJson() => {
        "lat": lat,
        "lng": lng,
      };
}

class Item {
  Item({
    this.id,
    this.name,
    this.price,
  });

  int? id;
  String? name;
  String? price;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        name: json["name"],
        price: json["price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
      };
}
