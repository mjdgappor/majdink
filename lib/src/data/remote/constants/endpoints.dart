class EndPoints {
  static const baseUrl = 'https://62f4b229ac59075124c1e40b.mockapi.io';
  static const int receiveTimeout = 50000;
  static const int connectionTimeout = 50000;
  static const getOrders = '/api/v1/orders';
}
