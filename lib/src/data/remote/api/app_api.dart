import 'package:get/get.dart';
import '../../models/orders_model.dart';
import '../../models/pagination_filter.dart';
import '../constants/endpoints.dart';
import 'dio_client.dart';

class AppApi {
  final DioClient _dioClient = Get.find();

  Future<OrderModel> getOrders({
    var data,
    PaginationFilter? paginationFilter,
  }) async {
    final res = await _dioClient.get(EndPoints.getOrders,
        queryParameters: {"page": paginationFilter!.page, "limit": 15});
    return OrderModel.fromJson(res);
  }
}
