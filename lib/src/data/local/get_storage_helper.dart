import '../models/order_model.dart';
import 'user_storage.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'constants/storages.dart';

class GetStorageHelper {
  final _getStorage = Get.find<GetStorage>();

  // Theme:------------------------------------------------------
  bool get isDarkMode {
    return _getStorage.read(Storages.is_dark_mode) ?? false;
  }

  changeBrightnessToDark(bool value) {
    return _getStorage.write(Storages.is_dark_mode, value);
  }

  // Language:---------------------------------------------------
  String get currentLanguage {
    return _getStorage.read(Storages.current_language) ?? 'en';
  }

  changeLanguage(String language) {
    return _getStorage.write(Storages.current_language, language);
  }

  //General:---------------------------------------------------

  List get favorites {
    return _getStorage.read(UserStorages.favorites) ?? [];
  }

  addToFavorites(Order order) {
    List list = [];
    list.addAll(favorites);
    list.add(order.toJson());
    _getStorage.write(UserStorages.favorites, list);
  }

  removeFromFavorites(Order order) {
    favorites.removeWhere((element) => Order.fromJson(element).id == order.id);
    _getStorage.write(UserStorages.favorites, favorites);
  }

  List get orders {
    return _getStorage.read(UserStorages.orders) ?? [];
  }

  addToOrders(List<Order> orders) {
    List list = [];
    for (var element in orders) {
      list.add(element.toJson());
    }
    _getStorage.write(UserStorages.orders, list);
  }

  removeFromOrders() {
    _getStorage.remove(UserStorages.orders);
  }

  int get total {
    return _getStorage.read(UserStorages.total) ?? 0;
  }

  saveTotal(int total) {
    _getStorage.write(UserStorages.total, total);
  }

  removeTotal() {
    _getStorage.remove(UserStorages.total);
  }
}
