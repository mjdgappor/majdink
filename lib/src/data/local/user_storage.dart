class UserStorages {
  UserStorages._();

  static const String favorites = "favorites";
  static const String orders = "orders";
  static const String total = "total";
}
