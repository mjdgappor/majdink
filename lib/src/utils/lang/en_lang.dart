const Map<String, String> english = {
  //Error
  'requestToAPIServerWasCancelled': 'Request to server was cancelled',
  'connectionTimeoutWithAPIServer': 'Connection timeout with  server',
  'connectionToAPIServerFailedDueToInternetConnection':
      'Connection to server failed due to internet connection',
  'serverError': 'Sorry, there is server maintenance Please try again later',
  'refresh': 'Refresh',

  //Home

  "noOrders": "No orders",
  "noOrdersInFavourites": "No orders in favourites",
  "pleaseMakeSureYourInternetConnection":
      "Please make sure your internet connection",
  "noMoreResults": "No more results",

  //TabBar
  "home": "Home",
  "favorites": "Favorites",

  //Order card
  "number": "Number",
  "date": "Date",
  "time": "Time",
  "total": "Total",

  //Detailes
  "back": "Back",
  "item": "Item",

  //Notification
  'order': "Order",
  "addedToFavourites": "Added to favourites"
};
