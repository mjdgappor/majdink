const Map<String, String> arabic = {
  //Error
  'requestToAPIServerWasCancelled': 'تم إلغاء الطلب إلى السيرفر',
  'connectionTimeoutWithAPIServer': 'انتهت مهلة الإتصال مع الخادم',
  'connectionToAPIServerFailedDueToInternetConnection':
      'تحقق من الإتصال بالأنترنت',
  'serverError': 'عذرا يوجد صيانة للسيرفر\n الرجاء المحاولة لاحقا',
  'refresh': 'تحديث',

  //Home

  "noOrders": "لا يوجد طلبات",
  "noOrdersInFavourites": "لا يوجد طلبات في المفضلة",
  "pleaseMakeSureYourInternetConnection": "الرجاء التحقق من الاتصال بالانترنت",
  "noMoreResults": "لا يوجد مزيد من النتائج",

  //TabBar
  "home": "الرئيسية",
  "favorites": "المفضلة",

  //Order card
  "number": "رقم",
  "date": "تاريخ",
  "time": "وقت",
  "total": "مجموع",

  //Detailes
  "back": "رجوع",
  "item": "عنصر",

  //Notification
  'order': "طلب",
  "addedToFavourites": "تم اضافته الى المفضلة"
};
