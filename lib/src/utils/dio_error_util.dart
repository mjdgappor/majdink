import 'package:dio/dio.dart';
import 'package:get/get.dart';
import '../configs/strings.dart';

class DioErrorUtil {
  static String handleError(DioError error) {
    String errorDescription = "";
    switch (error.type) {
      case DioErrorType.cancel:
        errorDescription = Strings.requestToAPIServerWasCancelled.tr;
        break;
      case DioErrorType.connectTimeout:
        errorDescription = Strings.connectionTimeoutWithAPIServer.tr;
        break;
      case DioErrorType.other:
        errorDescription =
            Strings.connectionToAPIServerFailedDueToInternetConnection.tr;
        break;
      case DioErrorType.receiveTimeout:
        errorDescription = "Receive timeout in connection with API server";
        break;
      case DioErrorType.response:
        error.response!.statusCode == 500
            ? errorDescription = Strings.serverError.tr
            : errorDescription =
                "Received invalid status code: ${error.response!.statusCode}";
        break;
      case DioErrorType.sendTimeout:
        errorDescription = "Send timeout in connection with API server";
        break;
    }

    return errorDescription;
  }
}
