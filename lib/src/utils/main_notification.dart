import 'package:get/get.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:test_inkcode/src/configs/strings.dart';
import '../ui/widgets/main_snackbar.dart';
import '../data/models/order_model.dart';

class MainNotification extends GetxController {
  AwesomeNotifications notification = Get.find();

  @override
  void onInit() {
    notification.isNotificationAllowed().then((value) {
      if (!value) AwesomeNotifications().requestPermissionToSendNotifications();
    });

    super.onInit();
  }

  void sendNotification(Order order) {
    notification.createNotification(
        content: NotificationContent(
      id: int.parse(order.id!),
      channelKey: "basic_channel",
      title: "${Strings.order.tr} ${order.id!} ${Strings.addedToFavourites.tr}",
    ));

    if (GetPlatform.isAndroid) {
      MainSnackBar.mainSnack(
          title:
              "${Strings.order.tr} ${order.id!} ${Strings.addedToFavourites.tr}",
          snackPosition: SnackPosition.TOP);
    }
  }
}
