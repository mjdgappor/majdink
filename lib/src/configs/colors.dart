import 'package:flutter/material.dart';

class AppColors {
  static Color blue = Color(0xff3d00eb);
  static Color red = Color(0xffff003d);
  static Color purpule = Color(0xff980099);
  static LinearGradient linearGradient = LinearGradient(
      colors: [blue, purpule, red],
      begin: Alignment.bottomRight,
      end: Alignment.topLeft);
}
