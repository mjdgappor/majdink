class Assets {
  Assets();

  //image
  static String appIcon = 'assets/images/letter-m.png';

  //sounds
  static String chimeupSound = 'sounds/chimeup.mp3';
}
