import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ThemesData {
  static ThemeData darkTheme = ThemeData.dark().copyWith(
    textTheme: GoogleFonts.notoSerifTextTheme().copyWith(
      bodyText2: GoogleFonts.notoSerif(
          textStyle: TextStyle(fontSize: 40, color: Colors.white)),
    ),
  );

  static ThemeData lightTheme = ThemeData.light().copyWith(
    backgroundColor: Colors.grey,
    textTheme: GoogleFonts.tajawalTextTheme().copyWith(
      bodyText2: GoogleFonts.tajawal(
          textStyle: TextStyle(
              fontSize: 14, color: Colors.grey, fontWeight: FontWeight.bold)),
    ),
  );
}
