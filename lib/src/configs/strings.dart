class Strings {
  //Error Strings
  static String requestToAPIServerWasCancelled =
      'requestToAPIServerWasCancelled';
  static String connectionTimeoutWithAPIServer =
      'connectionTimeoutWithAPIServer';
  static String connectionToAPIServerFailedDueToInternetConnection =
      'connectionToAPIServerFailedDueToInternetConnection';
  static String serverError = 'serverError';
  static String youMustBeLoggedIn = 'youMustBeLoggedIn';
  static String refresh = 'refresh';

  //Home strings
  static String noOrders = 'noOrders';
  static String noOrdersInFavourites = 'noOrdersInFavourites';
  static String pleaseMakeSureYourInternetConnection =
      'pleaseMakeSureYourInternetConnection';
  static String noMoreResults = 'noMoreResults';

  //TabBar strings
  static String home = 'home';
  static String favorites = 'favorites';

  //Order card strings
  static String number = 'number';
  static String date = 'date';
  static String time = 'time';
  static String total = 'total';

  //Detailes strings
  static String back = 'back';
  static String item = 'item';

  //Notification strings
  static String order = 'order';
  static String addedToFavourites = 'addedToFavourites';
}
