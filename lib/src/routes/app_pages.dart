import 'package:get/get.dart';
import '../ui/screens/detailes/detailes_page.dart';
import '../ui/screens/home/home_page.dart';

abstract class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: HomePage.route,
      page: () => HomePage(),
      transition: Transition.circularReveal,
    ),
    GetPage(
      name: DetailesPage.route,
      page: () => DetailesPage(),
      transition: Transition.circularReveal,
    ),
  ];
}
