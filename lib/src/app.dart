import 'configs/app_theme.dart';
import 'di/app_bindings.dart';
import 'ui/screens/home/home_page.dart';
import 'routes/app_pages.dart';
import 'utils/lang/translation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'data/local/get_storage_helper.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      builder: (context, child) {
        return MediaQuery(
            data: MediaQuery.of(context).copyWith(
              textScaleFactor: 1,
            ),
            child: child!);
      },
      title: 'Majd Jabbour',
      translations: TranslationPage(),
      fallbackLocale: Locale('en'),
      locale: Locale(Get.find<GetStorageHelper>().currentLanguage),
      theme: Get.find<GetStorageHelper>().isDarkMode
          ? ThemesData.darkTheme
          : ThemesData.lightTheme,
      home: HomePage(),
      initialRoute: HomePage.route,
      getPages: AppPages.pages,
      initialBinding: AppBinding(),
    );
  }
}
