import 'package:awesome_notifications/awesome_notifications.dart';
import '../data/local/get_storage_helper.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class InitBinding extends Bindings {
  @override
  Future dependencies() async {
    await Get.putAsync(() async => await GetStorage.init(), permanent: true);
    Get.lazyPut(() => GetStorageHelper(), fenix: true);
    Get.lazyPut(() => GetStorage(), fenix: true);

    await Get.putAsync(() async => await AwesomeNotifications().initialize(
        null,
        [
          NotificationChannel(
            channelKey: 'basic_channel',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel for basic tests',
          )
        ],
        debug: true));
  }
}
