import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import '../ui/screens/detailes/detailes_controller.dart';
import '../ui/screens/home/home_controller.dart';
import '../data/remote/api/app_api.dart';
import '../data/remote/api/dio_client.dart';
import '../data/remote/constants/endpoints.dart';
import '../data/repository.dart';
import '../utils/main_notification.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
        () => Dio(BaseOptions(
              baseUrl: EndPoints.baseUrl,
              receiveTimeout: EndPoints.receiveTimeout,
              connectTimeout: EndPoints.connectionTimeout,
            ))
              ..interceptors.add(LogInterceptor(
                error: true,
                request: true,
                responseBody: true,
                requestBody: true,
                requestHeader: true,
              )),
        fenix: true);

    Get.lazyPut(() => Repository(), fenix: true);
    Get.lazyPut(() => AppApi(), fenix: true);
    Get.lazyPut(() => DioClient(), fenix: true);
    Get.lazyPut(() => HomeController(), fenix: true);
    Get.lazyPut(() => DetailesController(), fenix: true);
    Get.lazyPut(() => AwesomeNotifications(), fenix: true);
    Get.lazyPut(() => MainNotification(), fenix: true);
  }
}
