import 'package:flutter/material.dart';
import 'src/app.dart';
import 'src/di/async_bindings.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await InitBinding().dependencies();
  runApp(App());
}
